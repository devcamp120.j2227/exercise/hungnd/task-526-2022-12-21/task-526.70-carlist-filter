import { useState } from "react";
import { Container, FormGroup, Label, Input, Row } from "reactstrap";

const carList = [
    {
      name: "BMW M6",
      url:
        "https://mediapool.bmwgroup.com/cache/P9/201411/P90169551/P90169551-the-new-bmw-m6-coup-exterior-12-2014-600px.jpg",
      release_year: 2020
    },
    {
      name: "VW Polo",
      url:
        "https://cdn.euroncap.com/media/30740/volkswagen-polo-359-235.jpg?mode=crop&width=359&height=235",
      release_year: 2018
    },
    {
      name: "Audi S6",
      url:
        "https://www.motortrend.com/uploads/sites/5/2020/03/6-2020-audi-s6.jpg?fit=around%7C875:492.1875",
      release_year: 2020
    },
    {
      name: "BMW M2",
      url:
        "https://imgd.aeplcdn.com/0x0/cw/ec/37092/BMW-M2-Exterior-141054.jpg?wm=0",
      release_year: 2019
    },
    {
      name: "Audi A3",
      url: "https://cdn.motor1.com/images/mgl/BEooZ/s3/2021-audi-s3.jpg",
      release_year: 2019
    }
  ];

export const CarListFilter =() =>{
    const [carType, setCarType] = useState("All");
    const [carYear, setCarYear] = useState(0);
    const selectTypeHandler =(event)=>{
        var carTypeSelect = event.target.value
        setCarType(carTypeSelect);
    }
    const handleYearChange=(event) =>{
        var yearSelect = event.target.value;
        console.log(yearSelect);
        setCarYear(yearSelect);
        // do whatever you want with isChecked value
      }
    const conditions = carList.filter(value => { 
        return value.release_year == carYear && value.name.toLowerCase().includes(carType.toLowerCase()); 
    });
    console.log(conditions)
    return(
        <Container className="mt-5">
            <FormGroup className="d-flex justify-content-center align-items-center">
                <Label>
                    <h3 style={{margin:"0px"}}>Filter By Brand: </h3>
                </Label>
                <Input
                name="select"
                type="select"
                onChange={selectTypeHandler}
                style={{width:"200px", marginLeft:"10px"}}>
                <option> All </option>
                <option> BMW </option>
                <option> AUDI </option>
                <option> VW </option>
                </Input>
            </FormGroup>
            <FormGroup className="mt-5 text-center" onChange={e => handleYearChange(e)}>
                <h3 style={{margin:"0px"}}>Filter By Year: </h3>
                <Label check> 2018 </Label>
                <Input value="2018" name="year" type="radio"/> &nbsp;
                <Label check> 2019 </Label>
                <Input name="year" type="radio" value="2019"/> &nbsp;
                <Label check> 2020 </Label>
                <Input name="year" type="radio" value="2020"/>
            </FormGroup>
            <Container>
                <Row xs="3" className="text-center">
    {/*điều kiện 1*/}   {carType !== "All" && conditions.length === 0 ? carList.filter(value => value.name.toLowerCase().includes(carType.toLowerCase())).map((value,index)=>{
                            return(
                            <div key={index} className="p-2" >
                                <img src={value.url} style={{width:"160px", height:"160px"}}/>
                                <div style={{marginTop:"10px"}}>
                                    <h6 style={{margin:"0px"}}>{value.name}</h6>
                                    <p style={{margin:"0px"}}>{value.release_year}</p>
                                </div>
                            </div>             
                        )})  //điều kiện 2 năm sản xuất khác 0
                        : carYear !== 0 && conditions.length === 0 ? carList.filter(value => value.release_year == carYear).map((value,index)=>{
                        return( 
                        <div key={index} className="p-2" >
                            <img src={value.url} style={{width:"160px", height:"160px"}}/>
                            <div style={{marginTop:"10px"}}>
                                <h6 style={{margin:"0px"}}>{value.name}</h6>
                                <p style={{margin:"0px"}}>{value.release_year}</p>
                            </div>
                        </div>             
                        )}) //điều kiện 3 khi cả 2 bộ lọc được ấn
                        : conditions.length > 0 ? 
                        conditions.map((e,index)=>{
                            return(
                            <div key={index} className="p-2" >
                                <img src={e.url} style={{width:"160px", height:"160px"}}/>
                                <div style={{marginTop:"10px"}}>
                                    <h6 style={{margin:"0px"}}>{e.name}</h6>
                                    <p style={{margin:"0px"}}>{e.release_year}</p>
                                </div>
                            </div>             
                        )})
                        : carList.map((value,index)=>{
                            return(
                            <div key={index} className="p-2" >
                                <img src={value.url} style={{width:"160px", height:"160px"}}/>
                                <div style={{marginTop:"10px"}}>
                                    <h6 style={{margin:"0px"}}>{value.name}</h6>
                                    <p style={{margin:"0px"}}>{value.release_year}</p>
                                </div>
                            </div>             
                        )})};
                </Row>
            </Container>
        </Container>
    )
}